# Vagrant (VM)

Vagrant is a modern alternative to MAMP/LAMP etc. and requires you to install VirtualBox (which is also free). The main benefits are easy switching between developer stacks and different versions of e.g. PHP. That way, you can easily mirror your production server for optimal testing. Other benefits are increased integration with _git_ and deployment. 

One downside is the 1GB RAM usage, so be sure to have enough.

## Quick start

Read this to get started: https://docs.vagrantup.com/v2/installation/index.html

## Extra

### Vagrant Manager
Desc: A useful Mac app that gives you access to popular terminal commands and a useful overview of your projects.

URL: http://vagrantmanager.com/

### Varying Vagrant Vagrants
Desc: An open source Vagrant configuration focused on WordPress development. You must install it in order to use the tools below.

URL: https://github.com/Varying-Vagrant-Vagrants/VVV

### Variable VVV
Desc: This makes creating new WP installations with a finished setup (plugins, themes etc.) a breeze. These finished setups are called "blueprints" and we have our own blueprints uploaded to this very repo. Write "_vv -c -b mekom_" to use our standard _blueprint_.

URL: https://github.com/bradp/vv

### VVV Dashboard
Desc: An overview of your sites, useful links, command etc.

URL: https://github.com/topdown/VVV-Dashboard